## 简介

Qt开发的一些例程，可以在Windows上运行，也可以在OK3568-C开发板上运行（SOC为rk3568）。关于OK3568-C的硬件介绍与软件开发环境的搭建，可参考以下两篇文章：

- [飞凌OK3568-C嵌入式Linux开发板开箱体验](https://www.bilibili.com/read/cv21222693)
- [RK3568源码编译与交叉编译环境搭建](https://www.bilibili.com/read/cv21222857)

> 本项目的Qt代码，主要从野火开发板的代码（<https://gitee.com/Embedfire/ebf_linux_qt_demo>）中移植修改过来，并进行进一步的整理与代码注释。

## 例程

### 01-MusicPlayer 音乐播放器

图文教程：[嵌入式Qt开发一个音乐播放器](https://www.bilibili.com/read/cv21809217)

![](pic/01.png)

### 02-VideoPlayer 视频播放器

![](pic/02.png)

### 0x-整理中...

## 推荐学习

- B站视频：[https://space.bilibili.com/146899653](https://gitee.com/link?target=https%3A%2F%2Fspace.bilibili.com%2F146899653)
- 个人博客：[https://xxpcb.gitee.io](https://xxpcb.gitee.io/) ,[https://xxpcb.github.io](https://gitee.com/link?target=https%3A%2F%2Fxxpcb.github.io)
- 知乎：[https://www.zhihu.com/people/xxpcb](https://gitee.com/link?target=https%3A%2F%2Fwww.zhihu.com%2Fpeople%2Fxxpcb)
- CSDN: [https://blog.csdn.net/hbsyaaa](https://gitee.com/link?target=https%3A%2F%2Fblog.csdn.net%2Fhbsyaaa)
- 微信公众文章：![img](pic/wxgzh.png)

感谢支持~

