
#ifndef _RKNN_SSD_PROCESS_H_
#define _RKNN_SSD_PROCESS_H_

#include <opencv2/opencv.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"

#include <rknn/rknn_api.h>
#include "rknn_ssd.h"

class RknnSsdModel
{
public:
    RknnSsdModel(){};
    ~RknnSsdModel(){};

    int RknnInit(const char *model_path);
    int RknnDeInit();
    unsigned char *LoadModel(const char *filename, int *model_size);
    int DoRknnSsd(cv::Mat &src, cv::Mat &res);

private:
    unsigned char *m_pModel = nullptr;
    rknn_context m_rknnCtx;
    rknn_input_output_num m_rknnIoNum;

};


#endif
