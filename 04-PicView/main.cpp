﻿#include "picviewwidget.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    PicViewWidget w;
    w.show();
    return a.exec();
}
