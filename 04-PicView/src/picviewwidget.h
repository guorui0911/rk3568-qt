﻿#ifndef PIC_VIEW_WIDGET_H
#define PIC_VIEW_WIDGET_H

#include <QWidget>
#include <QMainWindow>
#include <QImage>
#include <QPushButton>
#include <QLabel>
#include <QFileDialog>
#include <QDebug>
#include <QWheelEvent>
#include <QMouseEvent>
#include <QMoveEvent>

QT_BEGIN_NAMESPACE
namespace Ui { class PicViewWidget; }
QT_END_NAMESPACE

class ImageBox : public QWidget
{
public:
    ImageBox(QWidget *parent = nullptr);
    ~ImageBox();


    void setImage(QPixmap img);
private:
    void calcAndShow();

    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);
    void wheelEvent(QWheelEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

private:
    QPixmap m_img;
    QPoint m_point;
    int m_newWidth;
    int m_newHeight;
    int m_x = 0;
    int m_y = 0;
    float m_scale = 1.0;
    float m_lastScale = 1.0;
    bool m_wheelFlag = false;

    bool m_leftClick = false;
    QPoint m_startPos;
    QPoint m_endPos;
};

class PicViewWidget : public QMainWindow
{
    Q_OBJECT

public:
    PicViewWidget(QMainWindow *parent = nullptr);
    ~PicViewWidget();

private:
    QPushButton *m_nextBtn;
    QPushButton *m_prevBtn;
    QPushButton *m_openBtn;
    ImageBox *m_box;
    QString m_file;
    QStringList m_files;
    int m_idx = 0;

private slots:
    void show_next();     
    void show_prev();       
    void open_files();     
};
#endif // PIC_VIEW_WIDGET_H
