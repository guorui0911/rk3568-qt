﻿/******************************************************************
 Copyright (C) 2019 - All Rights Reserved by
 文 件 名 : widgettoolbar.h --- MusicToolBar
 作 者    : Niyh(lynnhua)
 论 坛    : http://www.firebbs.cn
 编写日期 : 2019
 说 明    :
 历史纪录 :
 <作者>    <日期>        <版本>        <内容>
  Niyh	   2019    	1.0.0 1     文件创建
 码农爱学习  2023/2/12                修改整理
*******************************************************************/
#ifndef MUSICTOOLBAR_H
#define MUSICTOOLBAR_H

#include "qtwidgetbase.h"
#include "qtsliderbar.h"

enum MUSIC_TOOL_BUTTON
{
    MUSIC_BTN_PLAY,
    MUSIC_BTN_PAUSE,
    MUSIC_BTN_PREV,
    MUSIC_BTN_NEXT,
    MUSIC_BTN_VOLUMN,
    MUSIC_BTN_LIST,
    MUSIC_BTN_NUM
};

///////////////////////////////////////////////////////////////////
/// \brief The MusicToolBar class
///
class MusicToolBar : public QtWidgetBase
{
    Q_OBJECT
public:
    explicit MusicToolBar(QWidget *parent = 0);
    ~MusicToolBar();

    void UpdateDurationInfo(int postion);
    void SetDuration(int duration);

    void SetPlayState(bool state);
signals:
    void toolBarClicked(int index);
    void currentPostionChanged(int postiion);

private:
    void InitWidget();

    int m_nMaxValue;
    int m_nCurrentValue;

    bool m_bPlaying;

    QtSliderBar *m_progressBar;
    QString m_timeFormat = "mm:ss";
    QString m_strCurrTime;
    QString m_strDuration;

private:
    void InitProperty();

    QTime GetTimeByPostion(int postion);
private slots:
    void SltBtnClicket(int index);

protected:
    void resizeEvent(QResizeEvent *e);
    void paintEvent(QPaintEvent *);
};
#endif // MUSICTOOLBAR_H
